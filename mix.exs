defmodule AudioMonitor.MixProject do
  use Mix.Project

  def project do
    [
      app: :audio_monitor,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {AudioMonitor.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:circuits_uart, "~> 1.3"},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
    ]
  end
end
