defmodule AudioMonitor.Reader do
  @moduledoc """
  Connect to the Nano BLE sense and collect sound values
  """
  use GenServer

  require Logger

  alias AudioMonitor.Parser

  def start_link(serial_number) do
    GenServer.start_link(__MODULE__, %{port_serial: serial_number}, name: __MODULE__)
  end

  def init(%{port_serial: serial_number}) do
    {:ok, pid} = Circuits.UART.start_link()

    {:ok, %{uart: pid, result: %AudioMonitor{}, serial_number: serial_number},
     {:continue, :setup_port}}
  end

  def handle_continue(:setup_port, %{uart: pid, serial_number: serial_number} = state) do
    ports = Circuits.UART.enumerate()

    case find_port(ports, serial_number) do
      {port, _} ->
        :ok =
          Circuits.UART.open(pid, port,
            speed: 9600,
            framing: {Circuits.UART.Framing.Line, separator: "\r\n"}
          )

        {:noreply, Map.put(state, :port, port)}

      _ ->
        Logger.error("Noise (Audio) monitor: no valid port found #{serial_number}")
        {:noreply, state}
    end
  end

  @doc """
  Helper function to find the right serial port
  given a serial number
  """
  def find_port(ports, serial_number) do
    Enum.find(ports, fn {_port, value} -> correct_port?(value, serial_number) end)
  end

  defp correct_port?(%{serial_number: number}, serial) do
    number == serial
  end

  defp correct_port?(%{}, _serial) do
    false
  end

  def process_data(data, pid) do
    result = Parser.parse(data)
    Process.send(pid, {:parser, result}, [])
  end

  def port, do: GenServer.call(__MODULE__, :port)

  def current_value, do: GenServer.call(__MODULE__, :current_value)

  def handle_call(:current_value, _from, %{result: result} = state) do
    {:reply, result, state}
  end

  def handle_call(:port, _from, %{port: port} = state) do
    {:reply, port, state}
  end

  def handle_info(:reconnect, state) do
    :ok = Circuits.UART.close(state[:uart])

    case Circuits.UART.open(state[:uart], state[:port],
           speed: 9600,
           framing: {Circuits.UART.Framing.Line, separator: "\r\n"}
         ) do
      :ok ->
        Logger.error("reconnected")

      {:error, msg} ->
        Logger.error("Audio Monitor: reconnect failed: #{inspect(msg)}")
        Process.send_after(self(), :reconnect, 500)
    end

    {:noreply, state}
  end

  def handle_info({:circuits_uart, port, {:error, msg}}, state) do
    Logger.error("Audio Monitor: resetting port: #{inspect(msg)}")

    if port == state[:port] do
      Process.send_after(self(), :reconnect, 100)
    end

    {:noreply, state}
  end

  def handle_info({:circuits_uart, port, data}, state) do
    if port == state[:port] do
      Task.start(__MODULE__, :process_data, [data, self()])
    end

    {:noreply, state}
  end

  def handle_info({:parser, result}, state) do
    {:noreply, Map.put(state, :result, result)}
  end
end
