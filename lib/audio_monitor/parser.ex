defmodule AudioMonitor.Parser do
  @moduledoc """
  Parse the data from the ardunino nano BLE sense
  """

  def parse(data) do
    %AudioMonitor{value: String.to_float(data)}
  end

end
